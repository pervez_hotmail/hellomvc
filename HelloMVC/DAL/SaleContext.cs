﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using HelloMVC.Models;

namespace HelloMVC.DAL
{
    public class SaleContext : DbContext
    {
        public DbSet<Product> Products { get; set; }

        public DbSet<PurchaseDetail> PurchaseDetails { get; set; }

        public DbSet<ServiceRepairDetail> ServiceRepairDetails { get; set; }

        public DbSet<PreviousOwner> PreviousOwners { get; set; }
    }
}