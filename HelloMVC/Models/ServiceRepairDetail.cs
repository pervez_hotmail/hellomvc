using System;

namespace HelloMVC.Models
{
    public class ServiceRepairDetail
    {
        public int ServiceRepairDetailId { get; set; }
        public ServiceRepairType ServiceRepairType { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
    }
}