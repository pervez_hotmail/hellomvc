using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HelloMVC.Models
{
    [ComplexType]
    public class Dimension
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int Depth { get; set; }
    }
}