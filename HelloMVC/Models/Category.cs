﻿using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelloMVC.Models
{
    //@TODO Need to test with different queries.
    public class Category
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Category ParentCategory { get; set; }
        public List<Category> SubCategories { get; set; }

        public List<Product> Products { get; set; }
    }
}
