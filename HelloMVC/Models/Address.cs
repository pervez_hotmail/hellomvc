using System.ComponentModel.DataAnnotations.Schema;

namespace HelloMVC.Models
{
    [ComplexType]
    public class Address
    {
        public string Location { get; set; } // city? street?
        public string State { get; set; }
    }
}