using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HelloMVC.Models
{
    public class PurchaseDetail
    {
        public int PurchaseDetailId { get; set; }
        public string MerchantName { get; set; }
        public Address Address { get; set; }
        public DateTime PurchaseDate { get; set; }
        public TimeSpan UsedFor { get; set; } // No DateSpan in .net. //needed? as today - purchase date == used for
        public decimal Price { get; set; }
        public int Warranty { get; set; } // years? months? datespan?
        public DateTime WarrantyEndsOn { get; set; }

        //WHATTT?
        public string ExtendedWarrantyDetailsName { get; set; }
        public decimal ExtendedWarranty { get; set; }
        
        public LoanInfo LoanInfo { get; set; }
    }
}