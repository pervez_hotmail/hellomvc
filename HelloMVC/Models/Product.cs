using System.Collections.Generic;

namespace HelloMVC.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public string DisplaySubject { get; set; }
        public string Name { get; set; }
        public string ModelName { get; set; }
        public string SerialNo { get; set; }
        public string ItemModelNo { get; set; }
        public string WebSite { get; set; }
        public decimal Weight { get; set; }

        public Dimension Dimension { get; set; }
        
        public Category Category { get; set; }
        public Category SubCategory { get; set; }

        public List<Tag> Tags { get; set; }

        public PurchaseDetail PurchaseDetail { get; set; }
        public List<ServiceRepairDetail> ServiceRepairDetails { get; set; }
        public List<PreviousOwner> PreviousOwners { get; set; }
    }
}