using System.ComponentModel.DataAnnotations.Schema;

namespace HelloMVC.Models
{
    [ComplexType]
    public class LoanInfo
    {
        public decimal LoanAmount { get; set; }
        public string LenderDetails { get; set; }
        public bool LoanCleared { get; set; }
        public decimal BalanceToBePaid { get; set; }
    }
}