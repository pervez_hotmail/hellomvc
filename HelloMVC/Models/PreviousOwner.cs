using System;

namespace HelloMVC.Models
{
    public class PreviousOwner
    {
        public int PreviousOwnerId { get; set; }
        public string Name { get; set; }
        public DateTime OwnershipPeriodStart { get; set; }
        public DateTime OwnershipPeriodEnd { get; set; }
    }
}