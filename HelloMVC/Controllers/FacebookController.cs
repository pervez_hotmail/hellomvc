﻿using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using Facebook;

namespace HelloMVC.Controllers
{
    public class FacebookController : Controller
    {
        //
        // GET: /Facebook/
        private readonly FacebookClient client;

        public FacebookController()
        {
            client = new FacebookClient("AAADubcDS8wMBAKnpnEyxJauVNpMRwOR23GqZATG1BHLCfty2YrQAuk1WCcu67SbSh1nvrbKVDZBAItTfrVDKzZCCYGdZAbkYovT3rXfmg1DLZCQZAl9h7H");
        }

        public ViewResult AllFriends()
        {
            
            return View((client.Get("/me/friends") as dynamic).data as IEnumerable);
        }

        public ViewResult TopPosts()
        {
            return View();
        }
        public ViewResult AboutFriend(string id)
        {
            var aboutFriend = client.Get("/" + id) as IDictionary<string, object>;
            return View(aboutFriend);
        }

        public ViewResult AboutMe()
        {
            return View();
        }


        public ViewResult CreatePost()
        {
            return View();
        }

        [HttpPost]
        public RedirectToRouteResult CreatePost(string post)
        {
            return RedirectToAction("TopPosts");
        }

    }


}
