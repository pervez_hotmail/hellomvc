﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HelloMVC.Models;
using HelloMVC.DAL;

namespace HelloMVC.Controllers
{
    public class PurchaseDetailController : Controller
    {
        private SaleContext db = new SaleContext();

        //
        // GET: /PurchaseDetail/

        public ActionResult Index()
        {
            return View(db.PurchaseDetails.ToList());
        }

        //
        // GET: /PurchaseDetail/Details/5

        public ActionResult Details(int id = 0)
        {
            PurchaseDetail purchasedetail = db.PurchaseDetails.Find(id);
            if (purchasedetail == null)
            {
                return HttpNotFound();
            }
            return View(purchasedetail);
        }

        //
        // GET: /PurchaseDetail/Create

        public PartialViewResult Create()
        {
            return PartialView();
        }

        //
        // POST: /PurchaseDetail/Create

        //[HttpPost]
        //public ActionResult Create(PurchaseDetail purchasedetail)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.PurchaseDetails.Add(purchasedetail);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(purchasedetail);
        //}

        //
        // GET: /PurchaseDetail/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PurchaseDetail purchasedetail = db.PurchaseDetails.Find(id);
            if (purchasedetail == null)
            {
                return HttpNotFound();
            }
            return View(purchasedetail);
        }

        //
        // POST: /PurchaseDetail/Edit/5

        [HttpPost]
        public ActionResult Edit(PurchaseDetail purchasedetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(purchasedetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(purchasedetail);
        }

        //
        // GET: /PurchaseDetail/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PurchaseDetail purchasedetail = db.PurchaseDetails.Find(id);
            if (purchasedetail == null)
            {
                return HttpNotFound();
            }
            return View(purchasedetail);
        }

        //
        // POST: /PurchaseDetail/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            PurchaseDetail purchasedetail = db.PurchaseDetails.Find(id);
            db.PurchaseDetails.Remove(purchasedetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}