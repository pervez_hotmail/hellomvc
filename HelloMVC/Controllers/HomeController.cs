﻿using System.Web.Mvc;

namespace HelloMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Buy, Sell Social Networking.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Girish cheppu babu, memento.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact.";

            return View();
        }
    }
}
