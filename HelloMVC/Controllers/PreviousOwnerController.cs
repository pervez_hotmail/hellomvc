﻿using System.Data;
using System.Linq;
using System.Web.Mvc;
using HelloMVC.Models;
using HelloMVC.DAL;

namespace HelloMVC.Controllers
{
    public class PreviousOwnerController : Controller
    {
        private readonly SaleContext db = new SaleContext();

        //
        // GET: /PreviousOwner/

        public ActionResult Index()
        {
            return View(db.PreviousOwners.ToList());
        }

        //
        // GET: /PreviousOwner/Details/5

        public ActionResult Details(int id = 0)
        {
            PreviousOwner previousowner = db.PreviousOwners.Find(id);
            if (previousowner == null)
            {
                return HttpNotFound();
            }
            return View(previousowner);
        }

        //
        // GET: /PreviousOwner/Create

        public PartialViewResult Create()
        {
            return PartialView();
        }

        //
        // POST: /PreviousOwner/Create

        //[HttpPost]
        //public ActionResult Create(PreviousOwner previousowner)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.PreviousOwners.Add(previousowner);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(previousowner);
        //}

        //
        // GET: /PreviousOwner/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PreviousOwner previousowner = db.PreviousOwners.Find(id);
            if (previousowner == null)
            {
                return HttpNotFound();
            }
            return View(previousowner);
        }

        //
        // POST: /PreviousOwner/Edit/5

        [HttpPost]
        public ActionResult Edit(PreviousOwner previousowner)
        {
            if (ModelState.IsValid)
            {
                db.Entry(previousowner).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(previousowner);
        }

        //
        // GET: /PreviousOwner/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PreviousOwner previousowner = db.PreviousOwners.Find(id);
            if (previousowner == null)
            {
                return HttpNotFound();
            }
            return View(previousowner);
        }

        //
        // POST: /PreviousOwner/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            PreviousOwner previousowner = db.PreviousOwners.Find(id);
            db.PreviousOwners.Remove(previousowner);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}