﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HelloMVC.Models;
using HelloMVC.DAL;

namespace HelloMVC.Controllers
{
    public class ServiceRepairController : Controller
    {
        //private SaleContext db = new SaleContext();

        ////
        //// GET: /ServiceRepair/

        //public ActionResult Index()
        //{
        //    return View(db.ServiceRepairDetails.ToList());
        //}

        ////
        //// GET: /ServiceRepair/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    ServiceRepairDetail servicerepairdetail = db.ServiceRepairDetails.Find(id);
        //    if (servicerepairdetail == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(servicerepairdetail);
        //}

        //
        // GET: /ServiceRepair/Create

        public PartialViewResult Create()
        {
            return PartialView();
        }

        //
        // POST: /ServiceRepair/Create

        //[HttpPost]
        //public ActionResult Create(ServiceRepairDetail servicerepairdetail)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.ServiceRepairDetails.Add(servicerepairdetail);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(servicerepairdetail);
        //}

        //
        // GET: /ServiceRepair/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    ServiceRepairDetail servicerepairdetail = db.ServiceRepairDetails.Find(id);
        //    if (servicerepairdetail == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(servicerepairdetail);
        //}

        ////
        //// POST: /ServiceRepair/Edit/5

        //[HttpPost]
        //public ActionResult Edit(ServiceRepairDetail servicerepairdetail)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(servicerepairdetail).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(servicerepairdetail);
        //}

        ////
        //// GET: /ServiceRepair/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    ServiceRepairDetail servicerepairdetail = db.ServiceRepairDetails.Find(id);
        //    if (servicerepairdetail == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(servicerepairdetail);
        //}

        ////
        //// POST: /ServiceRepair/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ServiceRepairDetail servicerepairdetail = db.ServiceRepairDetails.Find(id);
        //    db.ServiceRepairDetails.Remove(servicerepairdetail);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    db.Dispose();
        //    base.Dispose(disposing);
        //}
    }
}