using System;
using System.Data.Entity;
using HelloMVC.DAL;
using HelloMVC.Models;

namespace TestHelloMVC
{
    internal class SaleDatabaseInitializer : IDatabaseInitializer<SaleContext>
    {

        public void InitializeDatabase(SaleContext context)
        {
            if(context.Database.Exists())
            {
                context.Database.Delete();
            }
            context.Database.CreateIfNotExists();
            var category = new Category {Name = "Electronics"};
            context.Products.Add(
                new Product
                    {
                        DisplaySubject = "Rarely used New iPad WiFi only",
                        Name = "iPad 3",
                        ModelName = "iPad",
                        ItemModelNo = "123456",
                        SerialNo = "12345678",
                        Category = category,
                        SubCategory = new Category {Name = "Tablets", ParentCategory = category},
                        Dimension = new Dimension {Depth = 2, Width = 8, Height = 12},
                        PurchaseDetail =
                            new PurchaseDetail
                                {
                                    Address = new Address {Location = "Hyderabad", State = "Andhra Pradesh"},
                                    ExtendedWarranty = 2.0M,
                                    ExtendedWarrantyDetailsName = "Apple Extended Warranty",
                                    LoanInfo = new LoanInfo { BalanceToBePaid = 15000, LenderDetails = "SBI", LoanAmount = 30000, LoanCleared = false},
                                    MerchantName = "Reliancs iStore",
                                    Price = 30000,
                                    PurchaseDate = new DateTime(year:2012, month:7, day:31),
                                    UsedFor = new TimeSpan(days:100, hours: 0, minutes: 0, seconds: 0),
                                    Warranty = 3,
                                    WarrantyEndsOn = new DateTime(year:2015, month:7, day:31)
                                }
                    });
            context.SaveChanges();
        }
    }
}