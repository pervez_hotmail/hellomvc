﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelloMVC.DAL;
using Xunit;

namespace TestHelloMVC
{
    // @TODO Test cascade deletes
    public class ProductTest
    {
        public ProductTest()
        {
            Database.SetInitializer(new SaleDatabaseInitializer());
        }

        [Fact]
        public void TestSetup()
        {
            using(var ctx = new SaleContext())
            {
                Assert.Equal(1, ctx.Products.Count());
            }
        }
    }
}
